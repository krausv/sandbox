const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const path = require('path');
const webpack = require('webpack');


module.exports = (env, args) => {
	const isProduction = args.mode === 'production';
	return {
		context: path.resolve(__dirname),
		entry: {
			main: './www/scripts/index.js',
		},
		output: {
			path: path.join(__dirname, 'www/dist'),
			publicPath: isProduction ? '/dist/' : '/',
		},
		module: {
			rules: [
				{
					test: /\.css$/,
					use:[
						MiniCssExtractPlugin.loader,
						'css-loader',
					],
				}
			],
		},
		plugins: [
			new MiniCssExtractPlugin(),
		],
		devServer: {
			contentBase: path.join(__dirname, 'www/dist'),
			port: 3060
		},
		node: {
			fs: 'empty'
		}
	}
}