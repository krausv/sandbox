<?php

declare(strict_types=1);

namespace App\AdminModule\Presenters;

use Nette;
use Nittro\Bridges\NittroUI\Presenter;


/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends Presenter
{
	protected function startup() {
		parent::startup();
		$this->setDefaultSnippets(['header', 'content']);
	}
}
