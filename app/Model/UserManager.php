<?php

declare(strict_types=1);

namespace App\Model;

use App\Model\Facade\UsersFacade;
use Nette;
use Nette\Security\Passwords;


/**
 * Users management.
 */
final class UserManager implements Nette\Security\IAuthenticator
{
	use Nette\SmartObject;


	/** @var Passwords */
	private $passwords, $usersFacade;


	public function __construct(Passwords $passwords, UsersFacade $usersFacade)
	{
		$this->usersFacade = $usersFacade;
		$this->passwords = $passwords;
	}


	/**
	 * Performs an authentication.
	 *
	 * @param array $credentials
	 *
	 * @return Nette\Security\IIdentity
	 * @throws Nette\Security\AuthenticationException
	 */
	public function authenticate(array $credentials): Nette\Security\IIdentity
	{
		[$username, $password] = $credentials;
		$user = $this->usersFacade->getUserByUsername($username);

		if (!$user) {
			throw new Nette\Security\AuthenticationException('The username is incorrect.', self::IDENTITY_NOT_FOUND);
		} elseif (!$this->passwords->verify($password, $user->password)) {
			throw new Nette\Security\AuthenticationException('The password is incorrect.', self::INVALID_CREDENTIAL);
		} elseif ($this->passwords->needsRehash($user->password)) {
			$this->usersFacade->rehashPassword($this->passwords->hash($user->password), $user->id);
		}
		return new Nette\Security\Identity($user->id, explode(' ',$user->role), $this->prepareUsersData($user));
	}


	private function prepareUsersData($user): array
	{
		return [$user];
	}


	/**
	 * Adds new user.
	 *
	 * @param string $username
	 * @param string $email
	 * @param string $password
	 * @param null   $role
	 */
	public function add(string $username, string $email, string $password, $role = null): void
	{
		$this->usersFacade->create($username, $this->passwords->hash($password), $email, $role);
	}

}



class DuplicateNameException extends \Exception
{
}
