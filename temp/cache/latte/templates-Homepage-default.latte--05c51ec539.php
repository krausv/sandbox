<?php
// source: /Users/kraus/projects/sandbox/app/FrontModule/templates/Homepage/default.latte

use Latte\Runtime as LR;

class Template05c51ec539 extends Latte\Runtime\Template
{
	public $blocks = [
		'content' => 'blockContent',
		'title' => 'blockTitle',
	];

	public $blockTypes = [
		'content' => 'html',
		'title' => 'html',
	];


	function main()
	{
		extract($this->params);
		if ($this->getParentName()) return get_defined_vars();
		$this->renderBlock('content', get_defined_vars());
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		Nette\Bridges\ApplicationLatte\UIRuntime::initialize($this, $this->parentName, $this->blocks);
		Nittro\Bridges\NittroLatte\NittroRuntime::initialize($this);
		
	}


	function blockContent($_args)
	{
		extract($_args);
?>

<?php
		$this->renderBlock('title', get_defined_vars());
?>

<a class="ajax" href="<?php echo LR\Filters::escapeHtmlAttr($this->global->uiControl->link("flash!")) ?>">flash</a><?php
	}


	function blockTitle($_args)
	{
		extract($_args);
?><h1>Base project sandbox</h1>
<?php
	}

}
