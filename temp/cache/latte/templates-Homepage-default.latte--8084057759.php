<?php
// source: /Users/kraus/projects/sandbox/app/Presenters/templates/Homepage/default.latte

use Latte\Runtime as LR;

class Template8084057759 extends Latte\Runtime\Template
{
	public $blocks = [
		'content' => 'blockContent',
	];

	public $blockTypes = [
		'content' => 'html',
	];


	function main()
	{
		extract($this->params);
		if ($this->getParentName()) return get_defined_vars();
		$this->renderBlock('content', get_defined_vars());
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		Nette\Bridges\ApplicationLatte\UIRuntime::initialize($this, $this->parentName, $this->blocks);
		Nittro\Bridges\NittroLatte\NittroRuntime::initialize($this);
		
	}


	function blockContent($_args)
	{
		extract($_args);
?>

<a class="ajax" href="<?php echo LR\Filters::escapeHtmlAttr($this->global->uiControl->link("flash!")) ?>">flash</a><?php
	}

}
