<?php
// source: /Users/kraus/projects/sandbox/app/FrontModule/templates/@layout.latte

use Latte\Runtime as LR;

class Template5e5c82a241 extends Latte\Runtime\Template
{
	public $blocks = [
		'head' => 'blockHead',
		'_content' => 'blockContent',
		'scripts' => 'blockScripts',
	];

	public $blockTypes = [
		'head' => 'html',
		'_content' => 'html',
		'scripts' => 'html',
	];


	function main()
	{
		extract($this->params);
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">

	<title><?php
		if (isset($this->blockQueue["title"])) {
			$this->renderBlock('title', $this->params, function ($s, $type) {
				$_fi = new LR\FilterInfo($type);
				return LR\Filters::convertTo($_fi, 'html', $this->filters->filterContent('striphtml', $_fi, $s));
			});
			?> | <?php
		}
?>Nette Sandbox</title>

	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="<?php echo LR\Filters::escapeHtmlAttr($this->global->webpackAssetLocator->locateInPublicPath("main.css")) ?>">
    <?php
		if ($this->getParentName()) return get_defined_vars();
		$this->renderBlock('head', get_defined_vars());
?>
</head>

<body>
<div class="container nittro-transition-auto nittro-transition-bar">
<?php
		$_tmp = Nette\Utils\Html::el('div')->setId($this->global->uiControl->getParameterId('flashes'))->data('flash-inline', true);
		foreach($flashes as $_tmp2) $_tmp->create('p')->setClass(str_replace('%type%', $_tmp2->type, 'nittro-flash nittro-flash-inline nittro-flash-%type%'))->setText($_tmp2->message) ?>	<div class="alert alert-"<?php
		echo $_tmp->attributes();
		?>><?php echo $_tmp->getHtml() ?></div>
	<div<?php echo ' id="' . htmlSpecialChars($this->global->snippetDriver->getHtmlId('content')) . '"' ?>>
<?php $this->renderBlock('_content', $this->params) ?>
	</div>
</div>

<?php
		$this->renderBlock('scripts', get_defined_vars());
?>
</body>
</html>
<?php
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		$this->createTemplate('components/form.latte', $this->params, "import")->render();
		Nette\Bridges\ApplicationLatte\UIRuntime::initialize($this, $this->parentName, $this->blocks);
		Nittro\Bridges\NittroLatte\NittroRuntime::initialize($this);
		
	}


	function blockHead($_args)
	{
		
	}


	function blockContent($_args)
	{
		extract($_args);
		$this->global->snippetDriver->enter("content", "static");
		?>        <?php
		$this->renderBlock('content', $this->params, 'html');
?>

<?php
		$this->global->snippetDriver->leave();
		
	}


	function blockScripts($_args)
	{
		extract($_args);
		?>	<script src="<?php echo LR\Filters::escapeHtmlAttr($this->global->webpackAssetLocator->locateInPublicPath("main.js")) ?>"></script>
	<script src="<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 30 */ ?>/bower_components/nittro-full/dist/nittro.min.js" async defer></script>
<?php
	}

}
