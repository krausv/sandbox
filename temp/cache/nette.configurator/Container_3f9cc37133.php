<?php
// source: /Users/kraus/projects/sandbox/app/config/common.neon
// source: /Users/kraus/projects/sandbox/app/config/local.neon
// source: array

/** @noinspection PhpParamsInspection,PhpMethodMayBeStaticInspection */

declare(strict_types=1);

class Container_3f9cc37133 extends Nette\DI\Container
{
	protected $tags = [
		'nette.inject' => [
			'application.1' => true,
			'application.10' => true,
			'application.2' => true,
			'application.3' => true,
			'application.4' => true,
			'application.5' => true,
			'application.6' => true,
			'application.7' => true,
			'application.8' => true,
			'application.9' => true,
		],
		'kdyby.console.command' => [
			'migrations.continueCommand' => true,
			'migrations.createCommand' => true,
			'migrations.resetCommand' => true,
		],
		'nextras.migrations.extensionHandler' => [
			'migrations.extensionHandler.php' => ['for' => ['migrations'], 'extension' => 'php'],
			'migrations.extensionHandler.sql' => ['for' => ['migrations'], 'extension' => 'sql'],
		],
		'nextras.migrations.group' => [
			'migrations.group.basicData' => ['for' => ['migrations']],
			'migrations.group.dummyData' => ['for' => ['migrations']],
			'migrations.group.structures' => ['for' => ['migrations']],
		],
	];

	protected $types = ['container' => 'Nette\DI\Container'];

	protected $aliases = [
		'application' => 'application.application',
		'cacheStorage' => 'cache.storage',
		'httpRequest' => 'http.request',
		'httpResponse' => 'http.response',
		'nette.cacheJournal' => 'cache.journal',
		'nette.httpRequestFactory' => 'http.requestFactory',
		'nette.latteFactory' => 'latte.latteFactory',
		'nette.mailer' => 'mail.mailer',
		'nette.presenterFactory' => 'application.presenterFactory',
		'nette.templateFactory' => 'latte.templateFactory',
		'nette.userStorage' => 'security.userStorage',
		'router' => 'routing.router',
		'session' => 'session.session',
		'user' => 'security.user',
	];

	protected $wiring = [
		'Nette\DI\Container' => [['container']],
		'Nette\Application\Application' => [['application.application']],
		'Nette\Application\IPresenterFactory' => [['application.presenterFactory']],
		'Nette\Application\LinkGenerator' => [['application.linkGenerator']],
		'Nette\Caching\Storages\IJournal' => [['cache.journal']],
		'Nette\Caching\IStorage' => [['cache.storage']],
		'Nette\Http\RequestFactory' => [['http.requestFactory']],
		'Nette\Http\IRequest' => [['http.request']],
		'Nette\Http\Request' => [['http.request']],
		'Nette\Http\IResponse' => [['http.response']],
		'Nette\Http\Response' => [['http.response']],
		'Nette\Bridges\ApplicationLatte\ILatteFactory' => [['latte.latteFactory']],
		'Nette\Application\UI\ITemplateFactory' => [['latte.templateFactory']],
		'Nette\Mail\Mailer' => [['mail.mailer']],
		'Nette\Routing\RouteList' => [['routing.router']],
		'Nette\Routing\Router' => [['routing.router']],
		'Nette\Application\IRouter' => [['routing.router']],
		'ArrayAccess' => [
			2 => [
				'routing.router',
				'application.2',
				'application.3',
				'application.4',
				'application.6',
				'application.7',
				'application.8',
			],
		],
		'Countable' => [2 => ['routing.router']],
		'IteratorAggregate' => [2 => ['routing.router']],
		'Traversable' => [2 => ['routing.router']],
		'Nette\Application\Routers\RouteList' => [['routing.router']],
		'Nette\Security\Passwords' => [['security.passwords']],
		'Nette\Security\IUserStorage' => [['security.userStorage']],
		'Nette\Security\User' => [['security.user']],
		'Nette\Http\Session' => [['session.session']],
		'Tracy\ILogger' => [['tracy.logger']],
		'Tracy\BlueScreen' => [['tracy.blueScreen']],
		'Tracy\Bar' => [['tracy.bar']],
		'Nextras\Orm\Model\IRepositoryLoader' => [['nextras.orm.repositoryLoader']],
		'Nextras\Orm\Bridges\NetteDI\RepositoryLoader' => [['nextras.orm.repositoryLoader']],
		'Nette\Caching\Cache' => [2 => ['nextras.orm.cache']],
		'Nextras\Orm\Repository\IDependencyProvider' => [['nextras.orm.dependencyProvider']],
		'Nextras\Orm\Bridges\NetteDI\DependencyProvider' => [['nextras.orm.dependencyProvider']],
		'Nextras\Orm\Entity\Reflection\IMetadataParserFactory' => [['nextras.orm.metadataParserFactory']],
		'Nextras\Orm\Entity\Reflection\MetadataParserFactory' => [['nextras.orm.metadataParserFactory']],
		'Nextras\Orm\Model\MetadataStorage' => [['nextras.orm.metadataStorage']],
		'Nextras\Orm\Model\Model' => [['nextras.orm.model']],
		'Nextras\Orm\Model\IModel' => [['nextras.orm.model']],
		'App\Model\Orm\Orm' => [['nextras.orm.model']],
		'Nextras\Dbal\IConnection' => [['nextras.dbal.connection']],
		'Nextras\Dbal\Connection' => [['nextras.dbal.connection']],
		'Nextras\Migrations\IDbal' => [['migrations.dbal']],
		'Nextras\Migrations\IDriver' => [['migrations.driver']],
		'Nextras\Migrations\Entities\Group' => [
			2 => ['migrations.group.structures', 'migrations.group.basicData', 'migrations.group.dummyData'],
		],
		'Nextras\Migrations\IExtensionHandler' => [
			2 => ['migrations.extensionHandler.sql', 'migrations.extensionHandler.php'],
		],
		'Nextras\Migrations\Extensions\SqlHandler' => [2 => ['migrations.extensionHandler.sql']],
		'Nextras\Migrations\Extensions\PhpHandler' => [2 => ['migrations.extensionHandler.php']],
		'Nextras\Migrations\IConfiguration' => [['migrations.configuration']],
		'Nextras\Migrations\Bridges\SymfonyConsole\BaseCommand' => [
			2 => ['migrations.continueCommand', 'migrations.createCommand', 'migrations.resetCommand'],
		],
		'Symfony\Component\Console\Command\Command' => [
			2 => ['migrations.continueCommand', 'migrations.createCommand', 'migrations.resetCommand'],
		],
		'Nextras\Migrations\Bridges\SymfonyConsole\ContinueCommand' => [['migrations.continueCommand']],
		'Nextras\Migrations\Bridges\SymfonyConsole\CreateCommand' => [['migrations.createCommand']],
		'Nextras\Migrations\Bridges\SymfonyConsole\ResetCommand' => [['migrations.resetCommand']],
		'Oops\WebpackNetteAdapter\BasePath\BasePathProvider' => [2 => ['webpack.pathProvider.basePathProvider']],
		'Oops\WebpackNetteAdapter\PublicPathProvider' => [['webpack.pathProvider']],
		'Oops\WebpackNetteAdapter\BuildDirectoryProvider' => [['webpack.buildDirProvider']],
		'Oops\WebpackNetteAdapter\AssetLocator' => [['webpack.assetLocator']],
		'Oops\WebpackNetteAdapter\DevServer' => [['webpack.devServer']],
		'Oops\WebpackNetteAdapter\AssetNameResolver\AssetNameResolverInterface' => [
			['webpack.assetResolver.debug'],
			2 => ['webpack.assetResolver'],
		],
		'Oops\WebpackNetteAdapter\AssetNameResolver\DebuggerAwareAssetNameResolver' => [['webpack.assetResolver.debug']],
		'Nette\Security\IAuthenticator' => [['01']],
		'App\Model\UserManager' => [['01']],
		'App\Forms\FormFactory' => [['02']],
		'App\Forms\SignInFormFactory' => [['03']],
		'App\Forms\SignUpFormFactory' => [['04']],
		'App\Model\Facade\UsersFacade' => [['05']],
		'Nette\Application\IPresenter' => [
			2 => [
				'application.1',
				'application.2',
				'application.3',
				'application.4',
				'application.5',
				'application.6',
				'application.7',
				'application.8',
				'application.9',
				'application.10',
			],
		],
		'App\FrontModule\Presenters\ErrorPresenter' => [2 => ['application.1']],
		'App\FrontModule\Presenters\BasePresenter' => [2 => ['application.2', 'application.3', 'application.4']],
		'Nittro\Bridges\NittroUI\Presenter' => [
			2 => ['application.2', 'application.3', 'application.4', 'application.6', 'application.7', 'application.8'],
		],
		'Nette\Application\UI\Presenter' => [
			2 => ['application.2', 'application.3', 'application.4', 'application.6', 'application.7', 'application.8'],
		],
		'Nette\Application\UI\Control' => [
			2 => ['application.2', 'application.3', 'application.4', 'application.6', 'application.7', 'application.8'],
		],
		'Nette\Application\UI\Component' => [
			2 => ['application.2', 'application.3', 'application.4', 'application.6', 'application.7', 'application.8'],
		],
		'Nette\ComponentModel\Container' => [
			2 => ['application.2', 'application.3', 'application.4', 'application.6', 'application.7', 'application.8'],
		],
		'Nette\ComponentModel\Component' => [
			2 => ['application.2', 'application.3', 'application.4', 'application.6', 'application.7', 'application.8'],
		],
		'Nette\Application\UI\IRenderable' => [
			2 => ['application.2', 'application.3', 'application.4', 'application.6', 'application.7', 'application.8'],
		],
		'Nette\Application\UI\IStatePersistent' => [
			2 => ['application.2', 'application.3', 'application.4', 'application.6', 'application.7', 'application.8'],
		],
		'Nette\Application\UI\ISignalReceiver' => [
			2 => ['application.2', 'application.3', 'application.4', 'application.6', 'application.7', 'application.8'],
		],
		'Nette\ComponentModel\IContainer' => [
			2 => ['application.2', 'application.3', 'application.4', 'application.6', 'application.7', 'application.8'],
		],
		'Nette\ComponentModel\IComponent' => [
			2 => ['application.2', 'application.3', 'application.4', 'application.6', 'application.7', 'application.8'],
		],
		'App\FrontModule\Presenters\SignPresenter' => [2 => ['application.2']],
		'App\FrontModule\Presenters\HomepagePresenter' => [2 => ['application.3']],
		'App\FrontModule\Presenters\Error4xxPresenter' => [2 => ['application.4']],
		'App\AdminModule\Presenters\ErrorPresenter' => [2 => ['application.5']],
		'App\AdminModule\Presenters\BasePresenter' => [2 => ['application.6', 'application.7', 'application.8']],
		'App\AdminModule\Presenters\SignPresenter' => [2 => ['application.6']],
		'App\AdminModule\Presenters\HomepagePresenter' => [2 => ['application.7']],
		'App\AdminModule\Presenters\Error4xxPresenter' => [2 => ['application.8']],
		'NetteModule\ErrorPresenter' => [2 => ['application.9']],
		'NetteModule\MicroPresenter' => [2 => ['application.10']],
		'Nextras\Orm\Mapper\Dbal\DbalMapperCoordinator' => [['nextras.orm.mapperCoordinator']],
	];


	public function __construct(array $params = [])
	{
		parent::__construct($params);
		$this->parameters += [
			'appDir' => '/Users/kraus/projects/sandbox/app',
			'wwwDir' => '/Users/kraus/projects/sandbox/www',
			'vendorDir' => '/Users/kraus/projects/sandbox/vendor',
			'debugMode' => true,
			'productionMode' => false,
			'consoleMode' => false,
			'tempDir' => '/Users/kraus/projects/sandbox/app/../temp',
		];
	}


	public function createService01(): App\Model\UserManager
	{
		return new App\Model\UserManager($this->getService('security.passwords'), $this->getService('05'));
	}


	public function createService02(): App\Forms\FormFactory
	{
		return new App\Forms\FormFactory;
	}


	public function createService03(): App\Forms\SignInFormFactory
	{
		return new App\Forms\SignInFormFactory($this->getService('02'), $this->getService('security.user'));
	}


	public function createService04(): App\Forms\SignUpFormFactory
	{
		return new App\Forms\SignUpFormFactory($this->getService('02'), $this->getService('01'));
	}


	public function createService05(): App\Model\Facade\UsersFacade
	{
		return new App\Model\Facade\UsersFacade;
	}


	public function createServiceApplication__1(): App\FrontModule\Presenters\ErrorPresenter
	{
		return new App\FrontModule\Presenters\ErrorPresenter($this->getService('tracy.logger'));
	}


	public function createServiceApplication__10(): NetteModule\MicroPresenter
	{
		return new NetteModule\MicroPresenter($this, $this->getService('http.request'), $this->getService('routing.router'));
	}


	public function createServiceApplication__2(): App\FrontModule\Presenters\SignPresenter
	{
		$service = new App\FrontModule\Presenters\SignPresenter($this->getService('03'), $this->getService('04'));
		$service->injectPrimary(
			$this,
			$this->getService('application.presenterFactory'),
			$this->getService('routing.router'),
			$this->getService('http.request'),
			$this->getService('http.response'),
			$this->getService('session.session'),
			$this->getService('security.user'),
			$this->getService('latte.templateFactory')
		);
		$service->invalidLinkMode = 5;
		return $service;
	}


	public function createServiceApplication__3(): App\FrontModule\Presenters\HomepagePresenter
	{
		$service = new App\FrontModule\Presenters\HomepagePresenter;
		$service->injectPrimary(
			$this,
			$this->getService('application.presenterFactory'),
			$this->getService('routing.router'),
			$this->getService('http.request'),
			$this->getService('http.response'),
			$this->getService('session.session'),
			$this->getService('security.user'),
			$this->getService('latte.templateFactory')
		);
		$service->invalidLinkMode = 5;
		return $service;
	}


	public function createServiceApplication__4(): App\FrontModule\Presenters\Error4xxPresenter
	{
		$service = new App\FrontModule\Presenters\Error4xxPresenter;
		$service->injectPrimary(
			$this,
			$this->getService('application.presenterFactory'),
			$this->getService('routing.router'),
			$this->getService('http.request'),
			$this->getService('http.response'),
			$this->getService('session.session'),
			$this->getService('security.user'),
			$this->getService('latte.templateFactory')
		);
		$service->invalidLinkMode = 5;
		return $service;
	}


	public function createServiceApplication__5(): App\AdminModule\Presenters\ErrorPresenter
	{
		return new App\AdminModule\Presenters\ErrorPresenter($this->getService('tracy.logger'));
	}


	public function createServiceApplication__6(): App\AdminModule\Presenters\SignPresenter
	{
		$service = new App\AdminModule\Presenters\SignPresenter($this->getService('03'), $this->getService('04'));
		$service->injectPrimary(
			$this,
			$this->getService('application.presenterFactory'),
			$this->getService('routing.router'),
			$this->getService('http.request'),
			$this->getService('http.response'),
			$this->getService('session.session'),
			$this->getService('security.user'),
			$this->getService('latte.templateFactory')
		);
		$service->invalidLinkMode = 5;
		return $service;
	}


	public function createServiceApplication__7(): App\AdminModule\Presenters\HomepagePresenter
	{
		$service = new App\AdminModule\Presenters\HomepagePresenter;
		$service->injectPrimary(
			$this,
			$this->getService('application.presenterFactory'),
			$this->getService('routing.router'),
			$this->getService('http.request'),
			$this->getService('http.response'),
			$this->getService('session.session'),
			$this->getService('security.user'),
			$this->getService('latte.templateFactory')
		);
		$service->invalidLinkMode = 5;
		return $service;
	}


	public function createServiceApplication__8(): App\AdminModule\Presenters\Error4xxPresenter
	{
		$service = new App\AdminModule\Presenters\Error4xxPresenter;
		$service->injectPrimary(
			$this,
			$this->getService('application.presenterFactory'),
			$this->getService('routing.router'),
			$this->getService('http.request'),
			$this->getService('http.response'),
			$this->getService('session.session'),
			$this->getService('security.user'),
			$this->getService('latte.templateFactory')
		);
		$service->invalidLinkMode = 5;
		return $service;
	}


	public function createServiceApplication__9(): NetteModule\ErrorPresenter
	{
		return new NetteModule\ErrorPresenter($this->getService('tracy.logger'));
	}


	public function createServiceApplication__application(): Nette\Application\Application
	{
		$service = new Nette\Application\Application(
			$this->getService('application.presenterFactory'),
			$this->getService('routing.router'),
			$this->getService('http.request'),
			$this->getService('http.response')
		);
		$service->catchExceptions = false;
		$service->errorPresenter = 'Nette:Error';
		Nette\Bridges\ApplicationTracy\RoutingPanel::initializePanel($service);
		$this->getService('tracy.bar')->addPanel(new Nette\Bridges\ApplicationTracy\RoutingPanel(
			$this->getService('routing.router'),
			$this->getService('http.request'),
			$this->getService('application.presenterFactory')
		));
		return $service;
	}


	public function createServiceApplication__linkGenerator(): Nette\Application\LinkGenerator
	{
		return new Nette\Application\LinkGenerator(
			$this->getService('routing.router'),
			$this->getService('http.request')->getUrl()->withoutUserInfo(),
			$this->getService('application.presenterFactory')
		);
	}


	public function createServiceApplication__presenterFactory(): Nette\Application\IPresenterFactory
	{
		$service = new Nette\Application\PresenterFactory(new Nette\Bridges\ApplicationDI\PresenterFactoryCallback(
			$this,
			5,
			'/Users/kraus/projects/sandbox/app/../temp/cache/nette.application/touch'
		));
		$service->setMapping(['*' => 'App\*Module\Presenters\*Presenter', 'Error' => 'App\*Module\Presenters\*Presenter']);
		return $service;
	}


	public function createServiceCache__journal(): Nette\Caching\Storages\IJournal
	{
		return new Nette\Caching\Storages\SQLiteJournal('/Users/kraus/projects/sandbox/app/../temp/cache/journal.s3db');
	}


	public function createServiceCache__storage(): Nette\Caching\IStorage
	{
		return new Nette\Caching\Storages\FileStorage(
			'/Users/kraus/projects/sandbox/app/../temp/cache',
			$this->getService('cache.journal')
		);
	}


	public function createServiceContainer(): Container_3f9cc37133
	{
		return $this;
	}


	public function createServiceHttp__request(): Nette\Http\Request
	{
		return $this->getService('http.requestFactory')->fromGlobals();
	}


	public function createServiceHttp__requestFactory(): Nette\Http\RequestFactory
	{
		$service = new Nette\Http\RequestFactory;
		$service->setProxy([]);
		return $service;
	}


	public function createServiceHttp__response(): Nette\Http\Response
	{
		return new Nette\Http\Response;
	}


	public function createServiceLatte__latteFactory(): Nette\Bridges\ApplicationLatte\ILatteFactory
	{
		return new class ($this) implements Nette\Bridges\ApplicationLatte\ILatteFactory {
			private $container;


			public function __construct(Container_3f9cc37133 $container)
			{
				$this->container = $container;
			}


			public function create(): Latte\Engine
			{
				$service = new Latte\Engine;
				$service->setTempDirectory('/Users/kraus/projects/sandbox/app/../temp/cache/latte');
				$service->setAutoRefresh(true);
				$service->setContentType('html');
				Nette\Utils\Html::$xhtml = false;
				$service->onCompile[] = function ($engine) { Nittro\Bridges\NittroLatte\NittroMacros::install($engine->getCompiler()); };
				$service->addProvider('webpackAssetLocator', $this->container->getService('webpack.assetLocator'));
				$service->onCompile[] = function ($engine) { Oops\WebpackNetteAdapter\Latte\WebpackMacros::install($engine->getCompiler()); };
				return $service;
			}
		};
	}


	public function createServiceLatte__templateFactory(): Nette\Application\UI\ITemplateFactory
	{
		return new Nette\Bridges\ApplicationLatte\TemplateFactory(
			$this->getService('latte.latteFactory'),
			$this->getService('http.request'),
			$this->getService('security.user'),
			$this->getService('cache.storage'),
			null
		);
	}


	public function createServiceMail__mailer(): Nette\Mail\Mailer
	{
		return new Nette\Mail\SendmailMailer;
	}


	public function createServiceMigrations__configuration(): Nextras\Migrations\IConfiguration
	{
		return new Nextras\Migrations\Configurations\Configuration(
			[
				$this->getService('migrations.group.structures'),
				$this->getService('migrations.group.basicData'),
				$this->getService('migrations.group.dummyData'),
			],
			[
				'sql' => $this->getService('migrations.extensionHandler.sql'),
				'php' => $this->getService('migrations.extensionHandler.php'),
			]
		);
	}


	public function createServiceMigrations__continueCommand(): Nextras\Migrations\Bridges\SymfonyConsole\ContinueCommand
	{
		return new Nextras\Migrations\Bridges\SymfonyConsole\ContinueCommand(
			$this->getService('migrations.driver'),
			$this->getService('migrations.configuration')
		);
	}


	public function createServiceMigrations__createCommand(): Nextras\Migrations\Bridges\SymfonyConsole\CreateCommand
	{
		return new Nextras\Migrations\Bridges\SymfonyConsole\CreateCommand(
			$this->getService('migrations.driver'),
			$this->getService('migrations.configuration')
		);
	}


	public function createServiceMigrations__dbal(): Nextras\Migrations\IDbal
	{
		return new Nextras\Migrations\Bridges\NextrasDbal\NextrasAdapter($this->getService('nextras.dbal.connection'));
	}


	public function createServiceMigrations__driver(): Nextras\Migrations\IDriver
	{
		return new Nextras\Migrations\Drivers\MySqlDriver($this->getService('migrations.dbal'));
	}


	public function createServiceMigrations__extensionHandler__php(): Nextras\Migrations\Extensions\PhpHandler
	{
		return new Nextras\Migrations\Extensions\PhpHandler([]);
	}


	public function createServiceMigrations__extensionHandler__sql(): Nextras\Migrations\Extensions\SqlHandler
	{
		return new Nextras\Migrations\Extensions\SqlHandler($this->getService('migrations.driver'));
	}


	public function createServiceMigrations__group__basicData(): Nextras\Migrations\Entities\Group
	{
		$service = new Nextras\Migrations\Entities\Group;
		$service->name = 'basic-data';
		$service->enabled = true;
		$service->directory = '/Users/kraus/projects/sandbox/app/../migrations/basic-data';
		$service->dependencies = ['structures'];
		$service->generator = null;
		return $service;
	}


	public function createServiceMigrations__group__dummyData(): Nextras\Migrations\Entities\Group
	{
		$service = new Nextras\Migrations\Entities\Group;
		$service->name = 'dummy-data';
		$service->enabled = true;
		$service->directory = '/Users/kraus/projects/sandbox/app/../migrations/dummy-data';
		$service->dependencies = ['structures', 'basic-data'];
		$service->generator = null;
		return $service;
	}


	public function createServiceMigrations__group__structures(): Nextras\Migrations\Entities\Group
	{
		$service = new Nextras\Migrations\Entities\Group;
		$service->name = 'structures';
		$service->enabled = true;
		$service->directory = '/Users/kraus/projects/sandbox/app/../migrations/structures';
		$service->dependencies = [];
		$service->generator = null;
		return $service;
	}


	public function createServiceMigrations__resetCommand(): Nextras\Migrations\Bridges\SymfonyConsole\ResetCommand
	{
		return new Nextras\Migrations\Bridges\SymfonyConsole\ResetCommand(
			$this->getService('migrations.driver'),
			$this->getService('migrations.configuration')
		);
	}


	public function createServiceNextras__dbal__connection(): Nextras\Dbal\Connection
	{
		$service = new Nextras\Dbal\Connection([
			'driver' => 'mysqli',
			'host' => 'application.loc',
			'username' => 'root',
			'password' => 'root',
			'database' => 'test',
		]);
		$this->getService('tracy.blueScreen')->addPanel('Nextras\Dbal\Bridges\NetteTracy\BluescreenQueryPanel::renderBluescreenPanel');
		Nextras\Dbal\Bridges\NetteTracy\ConnectionPanel::install($service, true);
		return $service;
	}


	public function createServiceNextras__orm__cache(): Nette\Caching\Cache
	{
		return new Nette\Caching\Cache($this->getService('cache.storage'), 'nextras.orm');
	}


	public function createServiceNextras__orm__dependencyProvider(): Nextras\Orm\Bridges\NetteDI\DependencyProvider
	{
		return new Nextras\Orm\Bridges\NetteDI\DependencyProvider($this);
	}


	public function createServiceNextras__orm__mapperCoordinator(): Nextras\Orm\Mapper\Dbal\DbalMapperCoordinator
	{
		return new Nextras\Orm\Mapper\Dbal\DbalMapperCoordinator($this->getService('nextras.dbal.connection'));
	}


	public function createServiceNextras__orm__metadataParserFactory(): Nextras\Orm\Entity\Reflection\MetadataParserFactory
	{
		return new Nextras\Orm\Entity\Reflection\MetadataParserFactory;
	}


	public function createServiceNextras__orm__metadataStorage(): Nextras\Orm\Model\MetadataStorage
	{
		return new Nextras\Orm\Model\MetadataStorage(
			[],
			$this->getService('nextras.orm.cache'),
			$this->getService('nextras.orm.metadataParserFactory'),
			$this->getService('nextras.orm.repositoryLoader')
		);
	}


	public function createServiceNextras__orm__model(): App\Model\Orm\Orm
	{
		return new App\Model\Orm\Orm(
			[[], [], []],
			$this->getService('nextras.orm.repositoryLoader'),
			$this->getService('nextras.orm.metadataStorage')
		);
	}


	public function createServiceNextras__orm__repositoryLoader(): Nextras\Orm\Bridges\NetteDI\RepositoryLoader
	{
		return new Nextras\Orm\Bridges\NetteDI\RepositoryLoader($this, []);
	}


	public function createServiceRouting__router(): Nette\Application\Routers\RouteList
	{
		return App\Router\RouterFactory::createRouter();
	}


	public function createServiceSecurity__passwords(): Nette\Security\Passwords
	{
		return new Nette\Security\Passwords;
	}


	public function createServiceSecurity__user(): Nette\Security\User
	{
		$service = new Nette\Security\User($this->getService('security.userStorage'), $this->getService('01'));
		$this->getService('tracy.bar')->addPanel(new Nette\Bridges\SecurityTracy\UserPanel($service));
		return $service;
	}


	public function createServiceSecurity__userStorage(): Nette\Security\IUserStorage
	{
		return new Nette\Http\UserStorage($this->getService('session.session'));
	}


	public function createServiceSession__session(): Nette\Http\Session
	{
		$service = new Nette\Http\Session($this->getService('http.request'), $this->getService('http.response'));
		$service->setExpiration('14 days');
		return $service;
	}


	public function createServiceTracy__bar(): Tracy\Bar
	{
		return Tracy\Debugger::getBar();
	}


	public function createServiceTracy__blueScreen(): Tracy\BlueScreen
	{
		return Tracy\Debugger::getBlueScreen();
	}


	public function createServiceTracy__logger(): Tracy\ILogger
	{
		return Tracy\Debugger::getLogger();
	}


	public function createServiceWebpack__assetLocator(): Oops\WebpackNetteAdapter\AssetLocator
	{
		return new Oops\WebpackNetteAdapter\AssetLocator(
			$this->getService('webpack.buildDirProvider'),
			$this->getService('webpack.pathProvider'),
			$this->getService('webpack.assetResolver.debug')
		);
	}


	public function createServiceWebpack__assetResolver(): Oops\WebpackNetteAdapter\AssetNameResolver\AssetNameResolverInterface
	{
		return new Oops\WebpackNetteAdapter\AssetNameResolver\IdentityAssetNameResolver;
	}


	public function createServiceWebpack__assetResolver__debug(): Oops\WebpackNetteAdapter\AssetNameResolver\DebuggerAwareAssetNameResolver
	{
		return new Oops\WebpackNetteAdapter\AssetNameResolver\DebuggerAwareAssetNameResolver($this->getService('webpack.assetResolver'));
	}


	public function createServiceWebpack__buildDirProvider(): Oops\WebpackNetteAdapter\BuildDirectoryProvider
	{
		return new Oops\WebpackNetteAdapter\BuildDirectoryProvider(
			'/Users/kraus/projects/sandbox/www/dist',
			$this->getService('webpack.devServer')
		);
	}


	public function createServiceWebpack__devServer(): Oops\WebpackNetteAdapter\DevServer
	{
		return new Oops\WebpackNetteAdapter\DevServer(true, 'http://localhost:3060', 0.1, new GuzzleHttp\Client);
	}


	public function createServiceWebpack__pathProvider(): Oops\WebpackNetteAdapter\PublicPathProvider
	{
		$service = new Oops\WebpackNetteAdapter\PublicPathProvider(
			'dist/',
			$this->getService('webpack.pathProvider.basePathProvider'),
			$this->getService('webpack.devServer')
		);
		$this->getService('tracy.bar')->addPanel(new Oops\WebpackNetteAdapter\Debugging\WebpackPanel(
			$service,
			$this->getService('webpack.assetResolver.debug'),
			$this->getService('webpack.devServer')
		));
		return $service;
	}


	public function createServiceWebpack__pathProvider__basePathProvider(): Oops\WebpackNetteAdapter\BasePath\BasePathProvider
	{
		return new Oops\WebpackNetteAdapter\BasePath\NetteHttpBasePathProvider($this->getService('http.request'));
	}


	public function initialize()
	{
		$this->getService('tracy.bar')->addPanel(new Nette\Bridges\DITracy\ContainerPanel($this));
		(function () {
			$response = $this->getService('http.response');
			$response->setHeader('X-Powered-By', 'Nette Framework 3');
			$response->setHeader('Content-Type', 'text/html; charset=utf-8');
			$response->setHeader('X-Frame-Options', 'SAMEORIGIN');
			$response->setCookie('nette-samesite', '1', 0, '/', null, null, true, 'Strict');
		})();
		$this->getService('session.session')->exists() && $this->getService('session.session')->start();
		Tracy\Debugger::$editorMapping = [];
		Tracy\Debugger::getLogger()->mailer = [
			new Tracy\Bridges\Nette\MailSender($this->getService('mail.mailer'), null),
			'send',
		];
		$this->getService('session.session')->start();
		Tracy\Debugger::dispatch();
	}
}
